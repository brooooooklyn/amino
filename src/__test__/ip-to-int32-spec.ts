import { expect } from 'chai'

import { ipv4ToInt32 } from '../ip-to-int32'

describe('ip to int32 specs', () => {
  const expectResult = 2896692481

  it('should convert a normal ip', () => {
    const result = ipv4ToInt32('172.168.5.1')
    expect(result).to.equal(expectResult)
  })

  it('should throw if ip is not a string', () => {
    const fn = () => ipv4ToInt32(Number('172.168.5.1') as any)
    expect(fn).to.throw('typeof input ip must be string, but got: number')
  })

  it('should convert blank between number and dot', () => {
    const result = ipv4ToInt32('172.168 .5.1')
    expect(result).to.equal(expectResult)
  })

  it('should convert blanks between number and dot', () => {
    const result = ipv4ToInt32('172.168      .5.1')
    expect(result).to.equal(expectResult)
  })

  it('should convert blank between dot and number', () => {
    const result = ipv4ToInt32('172. 168.5.1')
    expect(result).to.equal(expectResult)
  })

  it('should convert blanks between dot and number', () => {
    const result = ipv4ToInt32('172.         168.5.1')
    expect(result).to.equal(expectResult)
  })

  it('should throw if blank between numbers', () => {
    const fn = () => ipv4ToInt32('172.1 68.5.1')
    expect(fn).to.throw('space can not between two numbers')
  })

  it('should throw if blanks between numbers', () => {
    const fn = () => ipv4ToInt32('172.1     68.5.1')
    expect(fn).to.throw('space can not between two numbers')
  })

  it('should throw if addres out of range', () => {
    const fn = () => ipv4ToInt32('172.168.282.1')
    expect(fn).to.throw('address is out of range')
  })

  // FIXME undefined behaviour
  describe('undefined behaviours', () => {
    it('ip startWith space should be converted', () => {
      const result = ipv4ToInt32(' 172.168.5.1')
      expect(result).to.equal(expectResult)
    })

    it('ip startWith spaces should be converted', () => {
      const result = ipv4ToInt32('    172.168.5.1')
      expect(result).to.equal(expectResult)
    })

    it('ip endWith space should be converted', () => {
      const result = ipv4ToInt32('172.168.5.1 ')
      expect(result).to.equal(expectResult)
    })

    it('ip endWith spaces should be converted', () => {
      const result = ipv4ToInt32('172.168.5.1    ')
      expect(result).to.equal(expectResult)
    })

    it('should throw if dot follow another dot', () => {
      const fn = () => ipv4ToInt32('172.168.5..1')
      expect(fn).to.throw('dot can not follow another dot')
    })

    it('should throw if ip end with dot', () => {
      const fn = () => ipv4ToInt32('172.168.5.1.')
      expect(fn).to.throw('ip can not endWith dot')
    })

    it('should throw if ip end with dot with space', () => {
      const fn = () => ipv4ToInt32('172.168.5.1. ')
      expect(fn).to.throw('ip can not endWith dot or dot with space')
    })

    it('should throw if ip end with dot with spaces', () => {
      const fn = () => ipv4ToInt32('172.168.5.1.           ')
      expect(fn).to.throw('ip can not endWith dot or dot with space')
    })
  })
})
