const enum STATE {
  Init,
  Dot,
  DotSpace,
  NumberSpace,
  Number,
}

const BLANK_CHAR_CODE = 32 // " ".charCodeAt(0)
const DOT_CHAR_CODE = 46 // ".".charCodeAt(0)

export const ipv4ToInt32 = (ip: string): number => {
  if (typeof ip !== 'string') {
    throw new TypeError(`typeof input ip must be string, but got: ${typeof ip}`)
  }
  let state = STATE.Init
  let numberBuffer = ''
  let index = 3
  const uint8 = new Uint8Array(4)
  for (const char of ip) {
    if (char.charCodeAt(0) === DOT_CHAR_CODE) {
      if (state === STATE.Dot) {
        throw new TypeError('dot can not follow another dot')
      }
      state = STATE.Dot
      const num = Number(numberBuffer)
      if (num > 255) {
        throw new TypeError(`address is out of range: ${num}`)
      }
      uint8[index] = num
      index -= 1
      numberBuffer = ''
      continue
    }
    if (char.charCodeAt(0) === BLANK_CHAR_CODE) {
      if (state === STATE.Number) {
        state = STATE.NumberSpace
      } else if (state === STATE.Dot) {
        state = STATE.DotSpace
      }
      continue
    }
    if (state === STATE.NumberSpace) {
      throw new TypeError('space can not between two numbers')
    }
    state = STATE.Number
    numberBuffer += char
  }
  if (state === STATE.Dot || state === STATE.DotSpace) {
    throw new TypeError('ip can not endWith dot or dot with space')
  }
  uint8[0] = Number(numberBuffer)

  return new Uint32Array(uint8.buffer)[0]
}
